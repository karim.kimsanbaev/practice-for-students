﻿using System;
using System.Collections.Generic;

namespace BasicCSharp3
{
    public interface IBook
    {
        string Author { get; }
        void Print();
    }

    class Program
    {
        static void Main(string[] args)
        {
            var book = (IBook) new Journal("Tom", "AAA");

            book.Print();

            
        }
    }

    public class Journal : IBook
    {
        private readonly IBook _book;

        public Journal(string name, string lastName)
        {
            _book = new Book(name, lastName);
        }

        public string Author => _book.Author;

        public void Print() => _book.Print();
    }

    public class Book : IBook
    {
        public string Text
        {
            get;
        } = "Text Some";

        public string Author { get; } = "Tom";

        private Book(string author)
        {
            Author = author;
        }

        public Book(string name, string lastName) : this($"{name} {lastName}")
        {
        }

        public override string ToString()
        {
            return $"Author: {Author}, Text: {Text}";
        }

        public void Print()
        {
            Console.WriteLine($"Book text: {this}");
        }
    }
}
