﻿using System;
using System.Collections.Generic;

namespace BasicCSharp5
{
    class Program
    {
        static void Main(string[] args)
        {
            var books = new List<IBook>();
            
            var book = new Book("Andrey", "Some Text");
            books.Add(book);

            var journal = new Journal();
            books.Add(journal);

            var scienceBook = new ScienceBook("Alena", "Science text");
            books.Add(scienceBook);

            Console.WriteLine($"Books");
            foreach (var b in books)
            {
                Console.WriteLine(b);
            }
        }
    }

    public interface IBook
    {
        string Author { get; }

        void Print();
    }

    public abstract class BookBase : IBook
    {
        public abstract string Author { get; }
        public void Print()
        {
            Console.WriteLine(ToString());
        }
    }

    public class ScienceBook : BookBase
    {
        private readonly IBook _book;

        public ScienceBook(string author, string text)
        {
            _book = new Book(author, text);
        }

        public override string Author => _book.Author;

        public override string ToString()
        {
            return _book + " Science";
        }
    }

    public class Journal : BookBase
    {
        public override string Author { get; }

        public override string ToString()
        {
            return $"{nameof(Author)}: {Author}";
        }
    }

    public class Book : BookBase
    {
        public override string Author { get; }

        private string Text { get; }

        public Book(string author, string text)
        {
            Text = text;
            Author = author;
        }

        public override string ToString()
        {
            return $"{nameof(Text)}: {Text}, {nameof(Author)}: {Author}";
        }
    }
}