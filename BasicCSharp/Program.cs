﻿using BasicCSharp;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BasicCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book("Hello printer", new Printer());
            var book2 = new Book("Hello printer2", new BinaryPrinter("Book.txt"));

            book.Print();
            book2.Print();
        }

        static void ReadBookTextFromBinaryFile(string fileName)
        {
            var binaryFormatter = new BinaryFormatter();
            var fileStream = new FileStream(fileName, FileMode.Open);
            var obj = binaryFormatter.Deserialize(fileStream);
        }

        public class Book
        {
            private string _text;
            private readonly IPrinter _printer;

            public Book(string text, IPrinter printer)
            {
                _text = text;
                _printer = printer;
            }

            public void Print()
            {
                _printer.Print(_text);
            }
        }

        public class Printer : IPrinter
        {
            public void Print(string text)
            {
                Console.WriteLine(text);
            }  
        }

        public interface IPrinter
        {
            void Print(string text);
        }

        public class BinaryPrinter : IPrinter
        {
            private readonly string _fileName;

            public BinaryPrinter(string fileName)
            {
                _fileName = fileName;
            }

            public void Print(string text)
            {
                var binaryFormatter = new BinaryFormatter();
                var file = new FileStream(_fileName, FileMode.OpenOrCreate);
                binaryFormatter.Serialize(file, 123);
            }
        }
    }
}
