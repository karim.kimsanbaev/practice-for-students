﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicCSharp
{
    public class Pistol
    {
        public string Name
        {
            get;
        }

        public int Ammo
        {
            get;
        }

        public double Damage
        {
            get;
        }

        public int Price;
        public double Accuracy; // range [0, 1]

        public Pistol(string name, int ammo, double damage)
        {
            Name = name;
            Ammo = ammo;
            Damage = damage;
        }
    }
}
