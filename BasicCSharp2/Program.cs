﻿using System;

namespace BasicCSharp2
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book("Andreev", "Text Test Test", new FilePrinter());
            book.Print();


            var book2 = new Book("File A", "File only", new ConsolePrinter());
            book2.Print();
        }
    }
}
