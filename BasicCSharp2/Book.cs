﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BasicCSharp2
{
    public class ConsolePrinter : IPrinter
    {
        public void Print(string author)
        {
            Console.WriteLine($"{author}");
        }
    }

    public class FilePrinter : IPrinter
    {
        public void Print(string author)
        {
            // Запись в файл
            Console.WriteLine($"{author} in file");
        }
    }

    public interface IPrinter
    {
        void Print(string author);
    }

    public class Book
    {
        public string Author { get; }
        public string Text { get; }

        private readonly IPrinter _printer;

        public Book(string author, string text, IPrinter printer)
        {
            Author = author;
            Text = text;

            _printer = printer;
        }

        public void Print()
        {
            _printer.Print(this.Author);
        }

    }
}
