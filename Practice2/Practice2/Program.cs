﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Enumeration;
using System.Linq;
using Newtonsoft.Json;

namespace Practice2
{
    public interface IStudent
    {
        string Name { get; }
        string Group { get; }
        int CountLessonsInDay { get; }
    }

    public interface IEditableStudent
    {
        string Name { get; set; }
        string Group { get; set;  }
        int CountLessonsInDay { get; set;  }
    }

    public class Student : IStudent, ICloneable, IEditableStudent
    {
        public static IStudent CreateStudent()
        {
            Console.WriteLine("Введите имя студента");
            var name = Console.ReadLine();
            
            Console.WriteLine($"Введите группу в которой учится студент '{name}'");
            var group = Console.ReadLine();
            
            Console.WriteLine($"Введите количeство пар в день у группы '{group}'");
            var countLessonsInDayInput = Console.ReadLine();
            var countLessonsInDay = int.Parse(countLessonsInDayInput);

            var student = new Student(name, group, countLessonsInDay);
            
            return student;
        }

        [JsonConstructor]
        private Student(string name, string group, int countLessonsInDay)
        {
            Name = name;
            Group = group;
            CountLessonsInDay = countLessonsInDay;
        }
        
        public string Name { get; set; }
        public string Group { get; set; }
        public int CountLessonsInDay { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Group)}: {Group}, {nameof(CountLessonsInDay)}: {CountLessonsInDay}";
        }

        public object Clone()
        {
            return new Student(Name, Group, CountLessonsInDay);
        }
    }

    public interface IMenu
    {
        void PrintAll(Queue<IStudent> students);
        void AddNew(Queue<IStudent> students);
        void Remove(Queue<IStudent> students);
        void EditStudent(Queue<IStudent> student);
        void SearchByField(Queue<IStudent> students);
    }

    public class Menu : IMenu
    {
        private Queue<IStudent> _students;

        public Menu(Queue<IStudent> students)
        {
            _students = students;
        }

        public void PrintAll(Queue<IStudent> students)
        {
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }
        }

        public void AddNew(Queue<IStudent> students)
        {
            var newStudent = Student.CreateStudent();
            students.Enqueue(newStudent);
        }

        public void Remove(Queue<IStudent> students)
        {
            students.Dequeue();
        }

        public void EditStudent(Queue<IStudent> students)
        {
            var student = students.Peek();
            if (student is IEditableStudent editableStudent)
            {
                editableStudent.Name = "123123";
            }
        }

        public void SearchByField(Queue<IStudent> students)
        {
            Console.WriteLine("Введите имя студентов, которых хотите найти");
            var searchStudentByName = Console.ReadLine();

            var found = students.Where(s => s.Name == searchStudentByName);
            var result = found.ToList();

            foreach (var student in result)
            {
                Console.WriteLine(student.ToString());
            }
        }
    }

    class Program
    {
        public static string FileName;
        
        static void Main(string[] args)
        {
            Console.WriteLine("Введите имя файла:");
            var fileName = "data"; //Console.ReadLine();
            FileName = fileName;
            var data = GetFileData(fileName);

            var students = CreateStudentModels(data);

            CallMenu(students);
        }

        private static string GetFileData(string fileName)
        {
            using var fileStream = File.Open(fileName, FileMode.OpenOrCreate);

            var reader = new StreamReader(fileStream);
            var data = reader.ReadToEnd();

            return data;
        }

        public class QueueStudents
        {
            public Queue<Student> Students { get; set; }
        }

        private static Queue<IStudent> CreateStudentModels(string data)
        {
            var obj = JsonConvert.DeserializeObject<QueueStudents>(data);

            if (obj != null)
            {
                var studentsEnumerable = obj.Students.OfType<IStudent>();
                var students = new Queue<IStudent>(studentsEnumerable);
                return students;
            }

            return new Queue<IStudent>();
        }

        private static void CallMenu(Queue<IStudent> students)
        {
            var menu = new Menu(students);

            while (true)
            {
                Console.WriteLine("1. Вывести студентов");
                Console.WriteLine("2. Добавить студента");
                Console.WriteLine("3. Удалить студента");
                Console.WriteLine("4. Редактировать данные студента");
                Console.WriteLine("5. Найти студента по характеристике");
                Console.WriteLine("6. Выход");

                var inputString = Console.ReadLine();
                var selectingAction = int.Parse(inputString);
                switch (selectingAction)
                {
                    case 1:
                        menu.PrintAll(students);
                        break;
                    case 2:
                        menu.AddNew(students);
                        break;
                    case 3:
                        menu.Remove(students);
                        break;
                    case 4:
                        menu.EditStudent(students);
                        break;
                    case 5:
                        menu.SearchByField(students);
                        break;
                    case 6:
                        SaveDataInFile(students, FileName);
                        Environment.Exit(0);
                        break;
                }
            }
        }

        private static void SaveDataInFile(Queue<IStudent> students, string fileName)
        {
            var serializeModel = new QueueStudents();
            var studentsStronglyTyped = students.OfType<Student>();

            serializeModel.Students = new Queue<Student>(studentsStronglyTyped);

            var data = JsonConvert.SerializeObject(serializeModel);
            
            using var fileStream = File.Open(fileName, FileMode.Create);
            using var writer = new StreamWriter(fileStream);
            
            writer.WriteLine(data);
        }
    }
}